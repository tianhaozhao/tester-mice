"""
Definations of Functions in the Attribution Module
====================
*Contents: Factor (Regression) classes, methods and results
*Version: 0.1
*Update Date: 2017-07-12
*Info:

====================
"""

import pandas as pd
import numpy as np
import statsmodels as sm
import statsmodels.formula.api as sm
import Cdef_RegMethod

def Compact(RawData, ret_var=['return'], fac_varlist=['sector','growth','size']):
    """
    To Create a compact DataFrame for coming procedures
    ===================
    *Require: 
            RawData (DataFrame): Dataset to compact
            ret_var= (str): A list of the name of return variable, with a length one only (if not, only the first element is used)
            fac_varlist= (list): A list consisting of variables for the regression, classified or continuous are all permitted
    *Output:
            DatCompact (DataFrame): A dataframe containing compact data
    *Info:
            1. DatCompact both contains continuous variables and classified variables. But the latter is decomposed to N singular 
                    dummy vars where N is the number of levels in a specific classified var. Decomposed vars are named in the 
                    form of "classified"+"levels", which follows a design in R's pa package
            2. The vars' order in DatCompact does not matter when we use the mat to compute the exposure, but may affect the
                    result while regression. For now, we apply a easy-change design to make dummys after continuous. If needed,
                    it could be redited at any time
    *Version: 0.1
    *Update: 2017-07-12
    """
    #Validation
    if len(ret_var)==1 & isinstance(RawData,pd.DataFrame):
        pass
    else:
        raise Exception(ValueError)

    #Re-arrangement of the dataset
    ret_var = ret_var[0] #reset ret_var to delete possible redundanct elements
        #DatMerged = pd.merge(RawData[ret_var],RawData[fac_varlist],left_index=True,right_index=True)
    Return_Series = pd.Series(RawData[ret_var])
    Factor_Dframe = RawData[fac_varlist]


    DatCompact = { "Return":Return_Series, "Factors":Factor_Dframe }


    return( DatCompact )

#===============================================#

def RegFac(Dat, ret_var="return", reg_var=['sector','size','value','growth'], benchmark_weight="benchmark", portfolio_weight="portfolio", date_var='date' ):
        """
        To do core regression for attribuition analysis, and return a dict as the result
        ==========================
        *Require: 
            Dat (DataFrame): Compact Dataset
            ret_var= (str): The name of return variable, with a length one only (if not, only the first element is used)
            fac_varlist= (list): A list consisting of variables for the regression, classified or continuous are all permitted
        *Output:
            Attr_Reg : A list contains everything you need for one-period
        *Info:
        *Version: 0.1
        *Update: 2017-07-12       

        """

        ########Multi-period Flow Control#########

        #Extract dates
        date_unique = pd.Series(Dat[date_var]).drop_duplicates()
        Length = len(date_unique)
        date_unique = date_unique.sort_values()

        if Length>1:

                #A list to store final result
                result_list = []
                #CountIdx = range(Length)
                #Zip_index = zip(CountIdx,date_unique)

                for tmp_y in date_unique:
                        #print(tmp_y)
                        result_list.append( RegFac(Dat[ Dat[date_var] == tmp_y ] ,
                                ret_var=ret_var,
                                reg_var=reg_var,
                                benchmark_weight=benchmark_weight,
                                portfolio_weight=portfolio_weight,
                                date_var=date_var
                                )
                        )
                
                #Dictionary Prepared
                Attr_dict = {
                        'date_var': date_var,
                        'ret_var': ret_var,
                        "reg_var": reg_var,
                        'benchmark_weight': benchmark_weight,
                        'portfolio_weight': portfolio_weight,
                        'universe': Dat
                }
                Value_dict = {
                        'coefficients': pd.DataFrame(),
                        'benchmark_ret': [],
                        'portfolio_ret': [],
                        'act_ret': [],
                        'act_expo': pd.DataFrame(),
                        'contrib': pd.DataFrame()
                }

                #benchmark return matrix pasted 
                tmp_zip = zip(range(len(result_list)), date_unique )
                for (IDX,DAT) in tmp_zip:
                        Value_dict['benchmark_ret'].insert(IDX, result_list[IDX].benchmark_ret )
                        Value_dict['portfolio_ret'].insert(IDX, result_list[IDX].portfolio_ret )
                        Value_dict['act_ret'].insert(IDX, result_list[IDX].act_ret )


                        Value_dict['coefficients'][str(DAT)] = result_list[IDX].coefficients
                        Value_dict['act_expo'][str(DAT)] = result_list[IDX].act_expo
                        Value_dict['contrib'][str(DAT)] = result_list[IDX].contrib
                for IDX in ['benchmark_ret','portfolio_ret','act_ret']:
                        Value_dict[IDX] = pd.Series( Value_dict[IDX], index=date_unique )

                #Create the result to return                
                Attr_Reg = Cdef_RegMethod.AttrReg( Attr_dict, Value_dict )

        else:
                ################Single-period Case##############
                #Validation
                if isinstance(ret_var, str) & isinstance(reg_var, list) & isinstance(benchmark_weight, str) & isinstance(portfolio_weight, str):
                        pass
                else:
                        raise ValueError

                #Returns computed
                benchmark_ret = np.dot(Dat[benchmark_weight], Dat[ret_var])
                portfolio_ret = np.dot(Dat[portfolio_weight], Dat[ret_var])

                #Active return computed
                act_ret = portfolio_ret - benchmark_ret

                #Regression
                lm_model = sm.OLS(Dat[ret_var], Dat[reg_var])
                lm_result = lm_model.fit()

                #Extract coef as factors
                factor_ret = lm_result.params

                #Active weight computed
                act_weight = Dat[portfolio_weight] - Dat[benchmark_weight]

                #Risk Exposure computed
                act_expo = pd.Series( np.dot(act_weight, Dat[reg_var]), index=reg_var )

                #Contribution (Score) computed
                contrib = np.multiply(act_expo, factor_ret)

                #Result dict created
                Attr_dict = {
                        'date_var': date_var,
                        'ret_var': ret_var,
                        "reg_var": reg_var,
                        'benchmark_weight': benchmark_weight,
                        'portfolio_weight': portfolio_weight,
                        'universe': Dat
                }

                Value_dict = {
                        'coefficients': factor_ret,
                        'benchmark_ret': benchmark_ret,
                        'portfolio_ret': portfolio_ret,
                        'act_ret': act_ret,
                        'act_expo': act_expo,
                        'contrib': contrib
                }

                Attr_Reg = Cdef_RegMethod.AttrReg(attr_dict=Attr_dict, value_dict=Value_dict)
                 
        #Final Return
        return(Attr_Reg)
