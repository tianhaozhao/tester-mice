import numpy as np
import pandas as pd
from imp import reload
import time
import statsmodels as sm
import statsmodels.formula.api as sm
#=====================================
import Fdef_RegMethod as Fdef
import Cdef_RegMethod as Cdef
#=====================================



#==========Data import============
data_quart = pd.read_csv('Quarter.csv')
data_jan = pd.read_csv('Jan.csv')
col_names = list( data_quart.columns )
#print(col_names,'\n=========')
#print(data_quart.head(5))
#=========Attr_dict preparasion========
    #生成回归的基本索引字典，reg_var要求全是连续变量／因子，或全是离散变量
attr_dict = {
    'date_var': 'date' ,
    'ret_var':  'return' ,
    'reg_var': ['size','growth','value'] ,
    'benchmark_weight': 'benchmark' ,
    'portfolio_weight': 'portfolio' 
}
#=========Attribution Analysis============
    #初始化对象并赋予基本的变量字典指定,初始化时的reg_var要求全是连续变量／因子
PA_sector = Cdef.AttrReg( data_quart, attr_dict )
    #如果在制订了reg_var全是离散变量的情形下考虑按sector分析，
    #则在回归前须在此更新哑变量化的数据集。但是如果不幸reg_var设置的成了连续变量....
    #Good luck to your equipment.....
    #joking! 其实已经加了有效性验证修复了23333333（除非故意要卡死它）
#PA_sector.sector_categorize()
    #运行回归分析，生成结果更新Value成员
PA_sector.RegFac()
#=================== Exposure =====================
tmp = PA_sector.exposure(var='sector')

#=================== Returns =======================
tmp_ret = PA_sector.returns( type='linking' )

#=================== Summary =======================

PA_sector.summary(outfolder='output/',outto='csv',rettype='geometric')



