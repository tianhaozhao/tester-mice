import numpy as np
import pandas as pd
from imp import reload
import time
import statsmodels as sm
import statsmodels.formula.api as sm
#=====================================
#import Fdef_RegMethod as Fdef
import Cdef_RegMethod as Cdef
#=====================================

#==========Data import============
data_quart = pd.read_csv('Quarter.csv')
data_jan = pd.read_csv('Jan.csv')

#=========Attr_dict preparasion========
    #生成回归的基本索引字典，reg_var要求要么全是连续变量／因子，要么全是离散变量
attr_dict = {
    'date_var': 'date' ,
    'ret_var':  'return' ,
    'cat_var':  'sector' ,
    'benchmark_weight': 'benchmark' ,
    'portfolio_weight': 'portfolio'
}

#=================== Initialization & Analysis ================
    #初始化对象并填充数据
pa = Cdef.AttrBHB( data_quart, attr_dict )
    #运行分析：经典 Brinson 方法
pa.Brinson()

#==================== Exposure Computation ====================
    #计算风险暴露，支持分类变量或连续变量，分类变量按分类给出结果，连续变量按5折分位数给出结果
    #此计算相对独立，不改变对象内容，不要求之前运行过分析，会返回一个列表（多期）或矩阵
tmp_expo = pa.exposure( var='size' )

#==================== Returns Computation =====================
    #计算不同方法下的收益分解，要求指定计算哪种方法的结果，多期的话需要指定均值计算方法
    #要求计算对应方法分解前必须运行对应的分析过程
    #会返回列表，不修改原对象
                    #===== Brinson =======
tmp_ret1 = pa.returns( method='Brinson',type='geometric' )
                    #===== Brinson Fachler =======
tmp_ret2 = pa.returns( method='BF',type='geometric' )                    
#==================== Summary ========================
    #总结打印，可以输出到屏幕或者csv文件
pa.summary( method='Brinson',outto='csv',outfolder='output/',multitype='geometric' )