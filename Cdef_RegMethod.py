"""
Definations of Classes in the Attribution Module
====================
*Contents: Factor (Regression) classes, methods and results
*Version: 0.1
*Update Date: 2017-07-12
*Info:

====================
"""
import numpy as np
import pandas as pd
from imp import reload
import time
import math
import statsmodels as sm
import statsmodels.formula.api as sm


class AttrReg:
    """
    Regression based Attribution Analysis ( Factor methods )
    Version: 0.5
    =================
    *It requires two params: a dictionary containing variables settings & a DataFrame to analysis

    *A sample dictionary of attr_dict parm:
        attr_dict = {
            'date_var': The column name of the var for date ,
            'ret_var':  The column name of the var for return ,
            "reg_var": A list, the column names of the vars for regression , must be all continuous or all discrete(classify/sector)
            'benchmark_weight': The column name of the var for weights in benchmark portfolio in decimals ,
            'portfolio_weight': The column name of the var for weights in portfolio in decimals ,
        }
        each entry is essential, but more another entries would be neglected
    =================
    """
    #========= Attributions ===========#

    #A dictionary storing basic information
    Info = {}
    #Values
    Value = {}
    #Data
    Data = None
    #Internal provess indicator, to tell the method RegFac whether the one-period computation is in the process but not a sinle task, if it is in, the method would return an new object 
    __indicator_iter = 0

    #========= Methods ==========#

    def __init__( self, DataIn , attr_dict ):
        """
        Initialization, requesting a dic containing attribuitons above
        """

        if not isinstance( attr_dict, dict ) & isinstance( DataIn, pd.DataFrame ):
            print("param: attr_dict shold be a dictionary or DataIn should be a DataFrame")
            raise ValueError
        else:
            self.Info = attr_dict
            self.Data = DataIn

        if not isinstance( attr_dict['reg_var'], list ):
            print( 'parms: reg_var should be a hashable type, list is acceptable' )    
            raise ValueError
        #To check dtypes of reg_var s, to ensure the regression work well
        reg = self.Info['reg_var']
        regvar_dtypes = []
        for i in reg:
            regvar_dtypes.append( DataIn[i].dtype )
        if len( pd.Series(regvar_dtypes).drop_duplicates() )>1:
            print( 'parms: variables in reg_var should have the same type, class or continuous ' )
            raise ValueError


        return(None)





    def summary(self, outto='monitor', outfolder='output/',rettype='geometric' ):
        """
        Type the result to working printers
            *If you want to select an outfolder= param, please have a '/' at the end. What's more, please ensure the path/folder selected exist
        """
        Dat = self.Data
        date_unique = pd.Series(self.Data[self.Info['date_var']]).drop_duplicates()
        Length = len( date_unique )
        date_unique.index = range(Length)

        if outto in ['monitor','Monitor']:
            #Title printing            
            print( '*Period: ',date_unique[0],' ~ ',date_unique[Length-1] )
            print('*Methodology: Regression (Factors)')
            SLOGAN_1 = '='*7
            SLOGAN_2 = ' '*7
            if Length > 1:
                print( '*Average Portfolio Size: ',float( Dat[Dat[self.Info['portfolio_weight']]>0][[self.Info['date_var'],self.Info['portfolio_weight']]].groupby(self.Info['date_var']).count().mean() )  )
                print( '*Average Benchmark Size: ',float( Dat[Dat[self.Info['benchmark_weight']]>0][[self.Info['date_var'],self.Info['benchmark_weight']]].groupby(self.Info['date_var']).count().mean() )  )
                print( SLOGAN_2,SLOGAN_1*4,'\n', SLOGAN_2,SLOGAN_1,' Returns ',SLOGAN_1,'\n',SLOGAN_2,SLOGAN_1*4,  sep='' )
                for (i,j) in self.returns( type=rettype ).items():
                    print( '===== ',i,' =====\n',j,sep='' )


            else:
                print( '*Portfolio Size: ',len(self.Data[self.Info['portfolio_weight']]) )
                print( '*Benchmark Size: ',len(self.Data[self.Info['benchmark_weight']]) )
                print( SLOGAN_2,SLOGAN_1*4,'\n', SLOGAN_2,SLOGAN_1,' Returns ',SLOGAN_1,'\n',SLOGAN_2,SLOGAN_1*4,  sep='' )
                print( self.returns( type=rettype ) )



        #Output to csv
        elif outto in ['csv','CSV']:
            if Length > 1:
                for (i,j) in self.returns( type=rettype ).items():
                    j.to_csv( outfolder + 'returns_' + i + '.csv' )
            else:
                self.returns( type=rettype ).to_csv( outfolder + 'returns_' + i + '.csv' )

    
        return(None)

    def returns(self,type='geometric'):
        """
        To compute the returns and return a mat/list
            Caution: this method askes for a pre-computation of .Brinson() or other ones
            type= is only for multi-period case, when only one-period, it will be neglected
                type has options: [ arithmetic, linking, geometric ]
                geometric is the default setting
            method= should be specified to tell the method which one result should be calculated
                method has options: [ Brinson, BF ]
        """
        if not isinstance( type, str ):
            print( 'parms: type= should be a string' )
            raise ValueError
        Dat = self.Data
        portfolio_weight = self.Info[ 'portfolio_weight' ]
        benchmark_weight = self.Info[ 'benchmark_weight' ]
        date_var = self.Info[ 'date_var' ]
        date_unique = pd.Series(Dat[date_var]).drop_duplicates()
        date_unique = date_unique.sort_values()
        Length = len( date_unique )
        date_unique.index = range(Length)
        tmp_regvar = self.Info['reg_var']
        
        #========= Brinson Method Based =========
        #Multi-period case
        if Length > 1:
            #Prepare dFrame
            idx_append = [ 'Residual','Portfolio Return','Benchmark Return','Active Return' ]
            tmp_regvar = self.Info['reg_var'].copy()
            tmp_regvar.extend(idx_append)
            raw = pd.DataFrame( index=tmp_regvar )
            #Binding
            for tmp_date in date_unique:
                tmp_AttrReg = AttrReg( Dat[ Dat[date_var]==tmp_date ],self.Info )
                tmp_AttrReg.RegFac()
                tmp_returns = tmp_AttrReg.returns( type=type )
                raw = raw.merge( tmp_returns,left_index=True,right_index=True )
            #print(raw) 
                
            #===== Compute avg with diff methods =====
            if type in ['arithmetic','Arithmetic']:
                ari_agg = pd.DataFrame( raw.sum(axis=1),
                        columns=[date_unique[Length-1]]
                )
                #Final
                return( {
                        'Raw': raw,
                        'Aggreate': ari_agg
                } )
                    
            elif type in ['linking','Linking']:
                portfolio_ret = (self.Value['portfolio_ret']+1).prod()
                benchmark_ret = (self.Value['benchmark_ret']+1).prod()
                act_ret = portfolio_ret - benchmark_ret
                A = act_ret / Length / ( portfolio_ret**(1/Length) - benchmark_ret**(1/Length) )
                C = ( act_ret - self.Value['act_ret'] * A ) / ( self.Value['act_ret']**2 ).sum()
                alpha = C * self.Value['act_ret']
                B_linking = A + alpha
                linking_raw = pd.DataFrame( columns=date_unique )
                for i in range(len( tmp_regvar )):
                    linking_raw = linking_raw.append( raw[i:i+1] * B_linking )
                
                linking_agg = pd.DataFrame( linking_raw.sum(axis=1), columns=[date_unique[Length-1]] )

                #Final
                return( { 
                    'Raw': linking_raw ,
                    'Aggerate': linking_agg
                } )

            elif type in ['geometric','Geometric']:
                agg = ( raw + 1 ).prod(axis=1) - 1
                new_actret = float(agg[-3:-2]) - float(agg[-2:-1])
                agg = agg[:-1].append( pd.Series(new_actret,index=['Active Return']) )
                #Final
                return( {
                    'Raw':raw ,
                    'Aggregate':agg
                } )

            else:
                print('parms: type= should be checked twice')
                raise ValueError


        #Sigle-period case
        else:
            idx_append = [ 'Residual','Portfolio Return','Benchmark Return','Active Return' ]
            #Prepare dFrame 
            ret_mat = pd.DataFrame( self.Value['contrib'],index=self.Info['reg_var'],columns=date_unique )
            #Addtional terms added
            tmp_ret1 = self.Value['act_ret'] - self.Value['contrib'].sum()
            tmp_ret2 = self.Value['portfolio_ret']
            tmp_ret3 = self.Value['benchmark_ret']
            tmp_ret4 = self.Value['act_ret']
            
            tmp_ret_series = pd.DataFrame( [tmp_ret1,tmp_ret2,tmp_ret3,tmp_ret4],index=idx_append,columns=date_unique )
            ret_mat = ret_mat.append( tmp_ret_series )
            #Final (dFrame)
            return( ret_mat )

            



    def exposure( self, var='sector' ):
        """
        To compute the risk exposure for specific variable ( not related to methods )
            requires var= to specify the variable to analyze
            not depend on specific analysis method
            Returns: 
                  **Multi-period case** : a list consisting of (in order of) portfolio exposure mat, benchmark expo mat and differencce between the two
                  **Single-period case** : a mat with columns : portfolio expo, benchmark expo and difference

        """
        if isinstance( var, str ):
            pass
        else:
            print( 'parms: var= should be a string' )
            raise ValueError

        Dat = self.Data
        portfolio_weight = self.Info[ 'portfolio_weight' ]
        benchmark_weight = self.Info[ 'benchmark_weight' ]
        date_var = self.Info[ 'date_var' ]
        date_unique = pd.Series(Dat[date_var]).drop_duplicates()
        date_unique = date_unique.sort_values()
        Length = len( date_unique )

        #Specify single period or multi periods
        #============= Multi periods case ================
        if Length > 1:
            #===== Classify case =====
            if isinstance(Dat[ var ][0], str):
                #Prepare empty dFrame
                expo_mat_port = pd.DataFrame(index=Dat[var].drop_duplicates())
                expo_mat_bench = pd.DataFrame(index=Dat[var].drop_duplicates())
                for no_date in date_unique:
                    expo_tmp_port = Dat[ Dat[date_var]==no_date ][ [ var, portfolio_weight ] ].copy().groupby( var ).sum()
                    expo_mat_port = expo_mat_port.merge( expo_tmp_port, left_index=True, right_index=True )    
                    expo_tmp_bench = Dat[ Dat[date_var]==no_date ][ [ var, benchmark_weight ] ].copy().groupby( var ).sum()
                    expo_mat_bench = expo_mat_bench.merge( expo_tmp_bench, left_index=True, right_index=True )         
                #Rename the columns by dates  
                expo_mat_port.columns = date_unique
                expo_mat_bench.columns = date_unique
                #Compute the diff
                expo_mat_diff = expo_mat_port - expo_mat_bench
                    #print( expo_mat_diff )
                #Fill the list
                    #expo_list = [ expo_mat_port, expo_mat_bench, expo_mat_diff ]
                #Final, return a dict
                return( {
                    'Exposure of Portfolio': expo_mat_port ,
                    'Exposure of Benchmark': expo_mat_bench ,
                    'Difference': expo_mat_diff
                } )

            #====== Continuous case ========
            else:
                range_level = [ '0 ~ 20%','20% ~ 40%','40% ~ 60%','60% ~ 80%','80% ~ 100%' ]
                #Prepare empty dFrame
                expo_mat_port = pd.DataFrame(index=range_level)
                expo_mat_bench = pd.DataFrame(index=range_level)
                for no_date in date_unique:
                    #Portfolio part
                    expo_tmp_port = Dat[ Dat[ date_var ] == no_date ][ [var, portfolio_weight] ].copy()
                    rank_q = np.array( expo_tmp_port[ var ].copy().rank().index ) +1
                    expo_tmp_port[ (var+'_group') ] = np.ceil( rank_q/len(rank_q) * 5 )
                    expo_tmp_port = expo_tmp_port.groupby( (var+'_group') ).sum()
                    expo_tmp_port.index = range_level
                    del expo_tmp_port[ var ]                    
                    expo_mat_port = expo_mat_port.merge( expo_tmp_port, left_index=True, right_index=True )
                    #Benchmark part
                    expo_tmp_bench = Dat[ Dat[ date_var ] == no_date ][ [var, benchmark_weight] ].copy()
                    rank_q = np.array( expo_tmp_bench[ var ].copy().rank().index ) +1
                    expo_tmp_bench[ (var+'_group') ] = np.ceil( rank_q/len(rank_q) * 5 )
                    expo_tmp_bench = expo_tmp_bench.groupby( (var+'_group') ).sum()
                    expo_tmp_bench.index = range_level
                    del expo_tmp_bench[ var ]                    
                    expo_mat_bench = expo_mat_bench.merge( expo_tmp_bench, left_index=True, right_index=True )
                    
                #Rename columns by dates
                expo_mat_port.columns = date_unique
                expo_mat_bench.columns = date_unique   
                #Compute the difference
                expo_mat_diff = expo_mat_port - expo_mat_bench
                    #print( expo_mat_port,'\n=====\n',expo_mat_bench,'\n=======\n',expo_mat_diff )
                #Fill the list
                    #expo_list = [ expo_mat_port, expo_mat_bench, expo_mat_diff ]
                #Final, return a dict
                return( {
                    'Exposure of Portfolio': expo_mat_port ,
                    'Exposure of Benchmark': expo_mat_bench ,
                    'Difference': expo_mat_diff
                } )
                
            #==================== Single period case ====================
        else: 
            #======= Classify case ========
            if isinstance(Dat[ var ][0], str):
                expo_mat = Dat[ [ var, portfolio_weight, benchmark_weight ] ].copy()
                expo_mat = expo_mat.groupby( var ).sum()
                expo_mat[ 'diff' ] = expo_mat[ portfolio_weight ] - expo_mat[ benchmark_weight ]
                    #print('expo_mat')
                return( expo_mat )

            #======= Continuous case =========
            else:
                #Grouping,leveling
                expo_mat = Dat[ [ var, portfolio_weight, benchmark_weight ] ].copy()
                rank_q =  np.array( Dat[ var ].copy().rank().index ) + 1
                expo_mat[ ( var + '_group' ) ] = np.ceil( rank_q/len(rank_q) * 5 )
                #Computing by groups
                expo_mat = expo_mat.groupby( ( var + '_group' ) ).sum()
                expo_mat[ 'diff' ] = expo_mat[ portfolio_weight ] - expo_mat[ benchmark_weight ]
                del expo_mat[ var ]
                expo_mat.index = [ '0 ~ 20%','20% ~ 40%','40% ~ 60%','60% ~ 80%','80% ~ 100%' ]
                    #print( expo_mat.head(5) )
                return( expo_mat )

        return(None)



    def sector_categorize(self):
        """
        Update the object to a form with dummy variables for discrete vars in reg_var list (only work in the totally discrete case)
            *The method will not return a copy, please have a backup before cateforizing
        """
        sector_var = self.Info['reg_var']
        #Check to prevent categorizing continous variables
        regvar_dtypes = []
        for i in sector_var:
            if str(self.Data[i].dtype) == 'float64':
                print( 'parms: variables to be categorized must be classify vars but not continous ones ' )
                raise ValueError

        Basic_list = [ self.Info['date_var'], self.Info['ret_var'], self.Info['benchmark_weight'], self.Info['portfolio_weight'] ]
        Basic_list.extend(sector_var)
        Dat = self.Data[ Basic_list ]
        reg_var = []
        for i in sector_var:
            DUMMY = pd.get_dummies( Dat.pop( i ),prefix= i )
            reg_var.extend( list(DUMMY.columns) )
            Dat = Dat.merge(DUMMY, left_index=True, right_index=True)

        self.Info['reg_var'] = reg_var
        self.Data = Dat

        return( None )


    def RegFac(self):
            """
            To do core regression for attribuition analysis, and return a dict as the result
            ==========================
            *Require: 
            Dat (DataFrame): Compact Dataset
            ret_var= (str): The name of return variable, with a length one only (if not, only the first element is used)
            fac_varlist= (list): A list consisting of variables for the regression, classified or continuous are all permitted
            *Output:
            Attr_Reg : A list contains everything you need for one-period
            *Info:
            *Version: 0.3
            *Update: 2017-07-17
            ==========================
            """
            tmp_Info = self.Info
            Dat = self.Data
            ret_var = tmp_Info['ret_var']
            reg_var= tmp_Info['reg_var']
            benchmark_weight = tmp_Info["benchmark_weight"]
            portfolio_weight = tmp_Info["portfolio_weight"]
            date_var = tmp_Info['date_var']

            ########Multi-period Flow Control#########

            #Extract dates
            date_unique = pd.Series(Dat[date_var]).drop_duplicates()
            Length = len(date_unique)
            date_unique = date_unique.sort_values()


            if Length>1:
                    #A list to store final result
                    result_list = []

                    #Core Step: Iteration
                    for tmp_y in date_unique:
                            #print(tmp_y)
                            One_period = AttrReg( Dat[ Dat[date_var] == tmp_y ], tmp_Info )
                            One_period._AttrReg__indicator_iter = 1
                            result_list.append( One_period.RegFac() )
                    
                    del One_period
                    #Dictionary Prepared
                    
                    Value_dict = {
                            'coefficients': pd.DataFrame(),
                            'benchmark_ret': [],
                            'portfolio_ret': [],
                            'act_ret': [],
                            'act_expo': pd.DataFrame(),
                            'contrib': pd.DataFrame()
                    }

                    #benchmark return matrix pasted 
                        #tmp_zip1 = zip(range(len(result_list)), date_unique ) 经查赋值后的zip似乎不能复用，是用过后就被删掉了么？
                    tmp_zip0 = zip(
                        ['benchmark_ret','portfolio_ret','act_ret'],
                        ['coefficients','act_expo','contrib']
                    )

                    for (FAC0,FAC1) in tmp_zip0:
                        for (IDX,DAT) in zip(range(len(result_list)), date_unique ):
                            Value_dict[FAC0].insert( IDX, result_list[IDX].Value[FAC0] )
                            Value_dict[FAC1][str(DAT)] = result_list[IDX].Value[FAC1]
                        #print('====== ',FAC1,' ======\n', Value_dict[FAC1])

                    for FAC0 in ['benchmark_ret','portfolio_ret','act_ret']:
                        Value_dict[FAC0] = pd.Series( Value_dict[FAC0], index=date_unique )
                        #print('========= ', FAC0, ' ========\n', Value_dict[FAC0] )

                    #Update the object
                    #Attr_Reg = AttrReg( Dat, tmp_Info, value_dict=Value_dict )

                    self.Data = Dat
                    self.Info = tmp_Info
                    self.Value = Value_dict                    
                    return(None)

            else:
                    ################Single-period Case##############
                    #Validation
                    if isinstance(ret_var, str) & isinstance(reg_var, list) & isinstance(benchmark_weight, str) & isinstance(portfolio_weight, str):
                            pass
                    else:
                            print("parms: (in single period) Parms validation fails ")
                            raise ValueError

                    #Returns computed
                    benchmark_ret = np.dot(Dat[benchmark_weight], Dat[ret_var])
                    portfolio_ret = np.dot(Dat[portfolio_weight], Dat[ret_var])

                    #Active return computed
                    act_ret = portfolio_ret - benchmark_ret

                    #Regression
                    lm_model = sm.OLS(Dat[ret_var], Dat[reg_var])
                    lm_result = lm_model.fit()

                    #Extract coef as factors
                    factor_ret = lm_result.params
                    
                    #Active weight computed
                    act_weight = Dat[portfolio_weight] - Dat[benchmark_weight]

                    #Risk Exposure computed
                    act_expo = pd.Series( np.dot(act_weight, Dat[reg_var]), index=reg_var )
                    
                    #Contribution (Score) computed
                    contrib = act_expo * factor_ret
                    
                    #Result dict created
                    Value_dict = {
                            'coefficients': factor_ret,
                            'benchmark_ret': benchmark_ret,
                            'portfolio_ret': portfolio_ret,
                            'act_ret': act_ret,
                            'act_expo': act_expo,
                            'contrib': contrib
                    }

                    #Trans NaN to zero
                    for i in Value_dict.keys():
                        if isinstance( Value_dict[i], pd.Series ):
                            Value_dict[ i ].fillna( 0, inplace=True )
                        else:
                            Value_dict[ i ] = np.nan_to_num( Value_dict[ i ] )

                    #Attr_Reg = AttrReg( Dat, tmp_Info, value_dict=Value_dict)
                    if self._AttrReg__indicator_iter == 1:
                        tmp_AttrReg = AttrReg( Dat, tmp_Info)
                        tmp_AttrReg.Value = Value_dict
                        return( tmp_AttrReg )
                    elif self._AttrReg__indicator_iter ==0:
                        #Update the object
                        #self.Data = Dat
                        #self.Info = tmp_Info
                        self.Value = Value_dict
                        return(None)
                    else:
                        print("parms: internal indicator error")
                        raise ValueError
                        
            #Final
            
############################################################################################################
############################################ Class End #####################################################
############################################################################################################



class AttrBHB:
    """
    Brinson, Brinson-Fachler models for Attribution Analysis
    Version: 0.1
    =================
    It requires two params: A DataFrame & a dictionary containing variables settings to analysis

    A sample dictionary of attr_dict parm:
        attr_dict = {
            'date_var': The column name of the var for date ,
            'ret_var':  The column name of the var for return ,
            'cat_var':  The column name of the category 
            'benchmark_weight': The column name of the var for weights in benchmark portfolio in decimals ,
            'portfolio_weight': The column name of the var for weights in portfolio in decimals ,
        }
        each entry is essential, but more another entries would be neglected
    =================    
    """
    #============= Attribuitions =================#
        #Dictionary to store variable settings
    Info = {}
        #A DataFrame containing the data
    Data = pd.DataFrame()
        #Dictionary to store the result (初始应当不指定)
    Value_Brinson = {}
        #Indicators for iteration
    __indicator_iter = 0

    #============= Methods =================#

    def __init__( self, DataIn, attr_dict ):
        """
        Initialization, requesting a dic containing atrribution above
        """
        if not isinstance( attr_dict, dict ):
            print("param: attr_dict shold be a dictionary")
            raise ValueError
        else:
            self.Info = attr_dict

        if not isinstance( DataIn, pd.DataFrame ):
            print("param: DataIn should be a DataFrame")
            raise ValueError
        else:
            self.Data = DataIn

        return(None)
    
    def exposure(self, var='sector' ):
        """
        To compute the risk exposure for specific variable ( not related to methods )
            requires var= to specify the variable to analyze
            not depend on specific analysis method
            Returns: 
                  **Multi-period case** : a list consisting of (in order of) portfolio exposure mat, benchmark expo mat and differencce between the two
                  **Single-period case** : a mat with columns : portfolio expo, benchmark expo and difference
        """
        if isinstance( var, str ):
            pass
        else:
            print( 'parms: var= should be a string' )
            raise ValueError

        Dat = self.Data
        portfolio_weight = self.Info[ 'portfolio_weight' ]
        benchmark_weight = self.Info[ 'benchmark_weight' ]
        date_var = self.Info[ 'date_var' ]
        date_unique = pd.Series(Dat[date_var]).drop_duplicates()
        date_unique = date_unique.sort_values()
        Length = len( date_unique )

        #Specify single period or multi periods
        #============= Multi periods case ================
        if Length > 1:
            #===== Classify case =====
            if isinstance(Dat[ var ][0], str):
                #Prepare empty dFrame
                expo_mat_port = pd.DataFrame(index=Dat[var].drop_duplicates())
                expo_mat_bench = pd.DataFrame(index=Dat[var].drop_duplicates())
                for no_date in date_unique:
                    expo_tmp_port = Dat[ Dat[date_var]==no_date ][ [ var, portfolio_weight ] ].copy().groupby( var ).sum()
                    expo_mat_port = expo_mat_port.merge( expo_tmp_port, left_index=True, right_index=True )    
                    expo_tmp_bench = Dat[ Dat[date_var]==no_date ][ [ var, benchmark_weight ] ].copy().groupby( var ).sum()
                    expo_mat_bench = expo_mat_bench.merge( expo_tmp_bench, left_index=True, right_index=True )         
                #Rename the columns by dates  
                expo_mat_port.columns = date_unique
                expo_mat_bench.columns = date_unique
                #Compute the diff
                expo_mat_diff = expo_mat_port - expo_mat_bench
                    #print( expo_mat_diff )
                #Fill the list
                    #expo_list = [ expo_mat_port, expo_mat_bench, expo_mat_diff ]
                #Final, return a dict
                return( {
                    'Exposure of Portfolio': expo_mat_port ,
                    'Exposure of Benchmark': expo_mat_bench ,
                    'Difference': expo_mat_diff
                } )

            #====== Continuous case ========
            else:
                range_level = [ '0 ~ 20%','20% ~ 40%','40% ~ 60%','60% ~ 80%','80% ~ 100%' ]
                #Prepare empty dFrame
                expo_mat_port = pd.DataFrame(index=range_level)
                expo_mat_bench = pd.DataFrame(index=range_level)
                for no_date in date_unique:
                    #Portfolio part
                    expo_tmp_port = Dat[ Dat[ date_var ] == no_date ][ [var, portfolio_weight] ].copy()
                    rank_q = np.array( expo_tmp_port[ var ].copy().rank().index ) +1
                    expo_tmp_port[ (var+'_group') ] = np.ceil( rank_q/len(rank_q) * 5 )
                    expo_tmp_port = expo_tmp_port.groupby( (var+'_group') ).sum()
                    expo_tmp_port.index = range_level
                    del expo_tmp_port[ var ]                    
                    expo_mat_port = expo_mat_port.merge( expo_tmp_port, left_index=True, right_index=True )
                    #Benchmark part
                    expo_tmp_bench = Dat[ Dat[ date_var ] == no_date ][ [var, benchmark_weight] ].copy()
                    rank_q = np.array( expo_tmp_bench[ var ].copy().rank().index ) +1
                    expo_tmp_bench[ (var+'_group') ] = np.ceil( rank_q/len(rank_q) * 5 )
                    expo_tmp_bench = expo_tmp_bench.groupby( (var+'_group') ).sum()
                    expo_tmp_bench.index = range_level
                    del expo_tmp_bench[ var ]                    
                    expo_mat_bench = expo_mat_bench.merge( expo_tmp_bench, left_index=True, right_index=True )
                    
                #Rename columns by dates
                expo_mat_port.columns = date_unique
                expo_mat_bench.columns = date_unique   
                #Compute the difference
                expo_mat_diff = expo_mat_port - expo_mat_bench
                    #print( expo_mat_port,'\n=====\n',expo_mat_bench,'\n=======\n',expo_mat_diff )
                #Fill the list
                    #expo_list = [ expo_mat_port, expo_mat_bench, expo_mat_diff ]
                #Final, return a dict
                return( {
                    'Exposure of Portfolio': expo_mat_port ,
                    'Exposure of Benchmark': expo_mat_bench ,
                    'Difference': expo_mat_diff
                } )
                
            #==================== Single period case ====================
        else: 
            #======= Classify case ========
            if isinstance(Dat[ var ][0], str):
                expo_mat = Dat[ [ var, portfolio_weight, benchmark_weight ] ].copy()
                expo_mat = expo_mat.groupby( var ).sum()
                expo_mat[ 'diff' ] = expo_mat[ portfolio_weight ] - expo_mat[ benchmark_weight ]
                    #print('expo_mat')
                return( expo_mat )

            #======= Continuous case =========
            else:
                #Grouping,leveling
                expo_mat = Dat[ [ var, portfolio_weight, benchmark_weight ] ].copy()
                rank_q =  np.array( Dat[ var ].copy().rank().index ) + 1
                expo_mat[ ( var + '_group' ) ] = np.ceil( rank_q/len(rank_q) * 5 )
                #Computing by groups
                expo_mat = expo_mat.groupby( ( var + '_group' ) ).sum()
                expo_mat[ 'diff' ] = expo_mat[ portfolio_weight ] - expo_mat[ benchmark_weight ]
                del expo_mat[ var ]
                expo_mat.index = [ '0 ~ 20%','20% ~ 40%','40% ~ 60%','60% ~ 80%','80% ~ 100%' ]
                    #print( expo_mat.head(5) )
                return( expo_mat )



    def summary(self, method='Brinson',outto='monitor',outfolder='output/',multitype='geometric'):
        """
        Print calculated results to monitor or other printers
            method: which model to summarize
            outto: device to output, monitor or csv
            outfolder: path to output, only works where outto='csv' or 'CSV'
            multitype: type for multi-brinson or carino methods
        """
        date_unique = pd.Series(self.Data[self.Info['date_var']]).drop_duplicates()
        Length = len( date_unique )
        date_unique.index = range(Length)
        #Print to screen
        if outto in ['None','monitor','Monitor']:
            #Title printing            
            print( '*Period: ',date_unique[0],' ~ ',date_unique[Length-1] )
            print( '*Portfolio Size: ', len(self.Data[self.Info['portfolio_weight']]) )
            print( '*Benchmark Size: ', len(self.Data[self.Info['benchmark_weight']]) )
                
            if method in ['brinson','Brinson']:
                print( '*Methodology: Brinson','\n','='*50,sep='' )
            elif method in ['Brinson-Fachler','BF']:
                print( '*Methodology: Brinson-Fachler','\n','='*50,sep='' )
            else:
                print( 'parms: method= shoud be checked twice' )
                raise ValueError

            SLOGAN_1 = '='*7
            SLOGAN_2 = ' '*7
            #Single? Multi?
            if Length >1 :
                #Print Exposures， according to Info dict
                print( SLOGAN_2,SLOGAN_1*4,'\n',SLOGAN_2,SLOGAN_1, ' Exposure ',SLOGAN_1,'\n',SLOGAN_2,SLOGAN_1*4 ,sep='')
                tmp = self.exposure( var=self.Info['cat_var'] )
                for (i,j) in tmp.items():
                    print( '======= ',i,' =======\n', j )
                #print returns
                print( SLOGAN_2,SLOGAN_1*4,'\n',SLOGAN_2,SLOGAN_1, ' Returns ',SLOGAN_1,'\n',SLOGAN_2,SLOGAN_1*4 ,sep='')
                tmp = self.returns(method=method)
                for (i,j) in tmp.items():
                    print( '======= Returns: ',i,' =======\n', j )
                
            else:
                #Print Exposures， according to Info dict
                print( SLOGAN_2,SLOGAN_1*4,'\n',SLOGAN_2,SLOGAN_1, ' Exposure ',SLOGAN_1,'\n',SLOGAN_2,SLOGAN_1*4 ,sep='')
                print( self.exposure( var=self.Info['cat_var'] ) )
                #print returns
                print( SLOGAN_2,SLOGAN_1*4,'\n',SLOGAN_2,SLOGAN_1, ' Returns ',SLOGAN_1,'\n',SLOGAN_2,SLOGAN_1*4 ,sep='')
                tmp = self.returns(method=method)
                for (i,j) in tmp.items():
                    print( '======= Returns: ',i,' =======\n', j )
                        
            #Final
            return(None)


        #Output to csv        
        elif outto in ['csv','CSV']:
            
            tmp_expo = self.exposure( var=self.Info['cat_var'] ) 
            #Compute returns by diff methods
            if method in ['brinson','Brinson']:
                tmp_ret = self.returns(method='Brinson')
            elif method in ['Brinson-Fachler','BF']:
                tmp_ret = self.returns(method='BF')
            else:
                print( 'parms: method= should be checked twice' )
                raise ValueError
            #Output returns
            for (i,j) in tmp_ret.items():
                j.to_csv( outfolder + 'returns_' + i + '.csv' )
            #Output exposure
            if Length>1:
                for (i,j) in tmp_expo.items():
                    j.to_csv( outfolder + 'exposure_' + i + '.csv' )
            else:
                tmp_expo.to_csv( outfolder + 'exposure_' + i + '.csv' )

            return(None)
                
        #Error drop
        else:
            print( 'parms: outto error' )
            raise ValueError
        #Final
        return( None )


  
    def returns( self, method='Brinson' , type='geometric' ):
        """
        To compute the returns and return a mat/list
            Caution: this method askes for a pre-computation of .Brinson() or other ones
            type= is only for multi-period case, when only one-period, it will be neglected
                type has options: [ arithmetic, linking, geometric ]
                geometric is the default setting
            method= should be specified to tell the method which one result should be calculated
                method has options: [ Brinson, BF ]
        """
        if not isinstance( type, str )&isinstance( method,str ) :
            print( 'parms: type= should be a string' )
            raise ValueError
        Dat = self.Data
        portfolio_weight = self.Info[ 'portfolio_weight' ]
        benchmark_weight = self.Info[ 'benchmark_weight' ]
        date_var = self.Info[ 'date_var' ]
        date_unique = pd.Series(Dat[date_var]).drop_duplicates()
        date_unique = date_unique.sort_values()
        Length = len( date_unique )
        date_unique.index = range(Length)
        
        #========= Brinson Method Based =========
        #METHOD: Basic Brinson
        if method in ['Brinson','brinson']:
            #Check whether the .Brinson was pre-runned 
            if self.Value_Brinson == {}:
                print( 'error: please run the .Brinson() method first' )
                raise ValueError

            #Multi-period case
            if Length > 1:
                #Data extract (all pd.Series)
                q1 = self.Value_Brinson[ 'midret_bench' ]
                q2 = self.Value_Brinson[ 'midret_allocation' ]
                q3 = self.Value_Brinson[ 'midret_selection' ]
                q4 = self.Value_Brinson[ 'midret_port' ] 
                brinson_mat = pd.DataFrame( [q4,q3,q2,q1],index=['q4','q3','q2','q1'] ) 
                #Effect calculates 
                active_return = q4 - q1
                allocation = q2 - q1
                selection = q3 - q1
                interaction = active_return - allocation - selection
                #Binding
                ari_raw = pd.DataFrame( [ allocation,selection,interaction,active_return ] ,
                                    index=[ 'Allocation','Selection','Interaction','Active Return' ] ,
                )
                
                #===== Compute avg with diff methods =====
                if type in ['arithmetic','Arithmetic']:
                    ari_agg = pd.DataFrame( ari_raw.sum(axis=1),
                            columns=[date_unique[Length-1]]
                    )
                    #Final
                    return( {
                            'Raw': ari_raw,
                            'Aggreate': ari_agg
                    } )
                    
                elif type in ['linking','Linking']:
                    port_ret_overtime = (brinson_mat+1).prod(axis=1)[0]
                    bench_ret_overtime = (brinson_mat+1).prod(axis=1)[3]
                    act_return = port_ret_overtime - bench_ret_overtime

                    T = brinson_mat.shape[1] #Time periods length

                    A_natural_scaling = act_return / T / ( ( port_ret_overtime )**(1/T) - (bench_ret_overtime)**(1/T) )
                    
                    C_tmp1 = act_return - A_natural_scaling * ( q4 - q1 ).sum()
                    C_tmp2 = ((q4 - q1)**2).sum()
                    C = C_tmp1 / C_tmp2

                    alpha = C * ( q4 - q1 )

                    B_linking = A_natural_scaling + alpha
                
                    linking_raw = ari_raw * B_linking #Automatically fit the index/columns
                    
                    linking_agg = pd.DataFrame( linking_raw.sum(axis=1) ,
                            columns=[date_unique[Length-1]]
                    )
                

                    #Final
                    return( { 
                        'Raw': linking_raw ,
                        'Aggerate': linking_agg
                     } )

                elif type in ['geometric','Geometric']:
                    tmp_mat = (brinson_mat+1).prod(axis=1)
                    allocation = tmp_mat[2] - tmp_mat[3] #q2 - q1
                    selection = tmp_mat[1] - tmp_mat[3] #q3 - q1
                    interaction = tmp_mat[0] - tmp_mat[1] - tmp_mat[2] + tmp_mat[2] #q4-q3-q2+q1
                    active_ret = tmp_mat[0] - tmp_mat[3] #q4-q1,port-bench
                    ret_mat = pd.DataFrame( [ allocation,selection,interaction,active_ret ] ,
                        index=['Allocation','Selection','Interaction','Active Return'],
                        columns=[date_unique[Length-1]]
                     )
                    #Final
                    return( {
                        'Raw':ari_raw ,
                        'Aggregate':ret_mat
                    } )

                else:
                    print('parms: type= should be checked twice')
                    raise ValueError


            #Sigle-period case
            else:
                #Data extract (all pd.Series)
                portret = self.Value_Brinson[ 'ret_port' ]
                benchret = self.Value_Brinson[ 'ret_bench' ]
                portwt = self.Value_Brinson[ 'weight_port' ]
                benchwt = self.Value_Brinson[ 'weight_bench' ]

                #Returns computation
                cat_allocation = (portwt - benchwt) * benchret
                cat_selection = (portret - benchret) * benchwt
                cat_interaction = (portret - benchret) * ( portwt - benchwt )

                #Result binding (the result is categorized by variable, .Info['cat_var'])
                cat_ret = pd.DataFrame( [ cat_allocation,cat_selection,cat_interaction ] ).T
                cat_ret.columns = [ 'Allocation', 'Selection', 'Interaction' ]
                #Add a total summary to the end row
                cat_ret = cat_ret.append( pd.DataFrame(cat_ret.sum(axis=0),columns=['Total']).T )

                #Overall Brinson Attribution
                q1 = self.Value_Brinson[ 'midret_bench' ]
                q2 = self.Value_Brinson[ 'midret_allocation' ]
                q3 = self.Value_Brinson[ 'midret_selection' ]
                q4 = self.Value_Brinson[ 'midret_port' ]
                ret_mat = pd.DataFrame( [q2-q1,q3-q1,q4-q3-q2+q1,q4-q1],columns=['Returns'],index=[ "Allocation Effect","Selection Effect","Interaction Effect","Active Return" ] )  
                
                #Return a dict
                return( {   
                    'Attribution by category in bps': cat_ret * 10000 ,
                    'Aggregate': ret_mat
                } )
            
        #========= Brinson-Fachler Method Based ==========
        elif method in ['BF','bf','Brinson-Fachler','brinson-falcher']:
            #Multi-period case
            if Length > 1:
                #Data extract (all pd.Series)
                q1 = self.Value_Brinson[ 'midret_bench' ]
                q2 = self.Value_Brinson[ 'midret_allocation' ]
                q3 = self.Value_Brinson[ 'midret_selection' ]
                q4 = self.Value_Brinson[ 'midret_port' ] 
                brinson_mat = pd.DataFrame( [q4,q3,q2,q1],index=['q4','q3','q2','q1'] ) 
                #Effect calculates (Adjusted)
                active_return = q4 - q1
                allocation = q2 - q1
                selection = q4 - q2
                    #interaction = active_return - allocation - selection
                #Binding (Adjusted)
                ari_raw = pd.DataFrame( [ allocation,selection,active_return ] ,
                                    index=[ 'Allocation','Selection','Active Return' ] ,
                )
                
                #===== Compute avg with diff methods =====
                if type in ['arithmetic','Arithmetic']:
                    ari_agg = pd.DataFrame( ari_raw.sum(axis=1),
                            columns=[date_unique[Length-1]]
                    )
                    #Final
                    return( {
                            'Raw': ari_raw,
                            'Aggreate': ari_agg
                    } )
                    
                elif type in ['linking','Linking']:
                    port_ret_overtime = (brinson_mat+1).prod(axis=1)[0]
                    bench_ret_overtime = (brinson_mat+1).prod(axis=1)[3]
                    act_return = port_ret_overtime - bench_ret_overtime

                    T = brinson_mat.shape[1] #Time periods length

                    A_natural_scaling = act_return / T / ( ( port_ret_overtime )**(1/T) - (bench_ret_overtime)**(1/T) )
                    
                    C_tmp1 = act_return - A_natural_scaling * ( q4 - q1 ).sum()
                    C_tmp2 = ((q4 - q1)**2).sum()
                    C = C_tmp1 / C_tmp2

                    alpha = C * ( q4 - q1 )

                    B_linking = A_natural_scaling + alpha
                
                    linking_raw = ari_raw * B_linking #Automatically fit the index/columns
                    
                    linking_agg = pd.DataFrame( linking_raw.sum(axis=1) ,
                            columns=[date_unique[Length-1]]
                    )
                
                    #Final
                    return( { 
                        'Raw': linking_raw ,
                        'Aggerate': linking_agg
                     } )

                elif type in ['geometric','Geometric']:
                    #(Adjusted)
                    tmp_mat = (brinson_mat+1).prod(axis=1)
                    allocation = tmp_mat[2] - tmp_mat[3] #q2 - q1
                    selection = tmp_mat[0] - tmp_mat[2] #q4 - q2
                        #interaction = tmp_mat[0] - tmp_mat[1] - tmp_mat[2] + tmp_mat[2] #q4-q3-q2+q1
                    active_ret = tmp_mat[0] - tmp_mat[3] #q4-q1,port-bench
                    ret_mat = pd.DataFrame( [ allocation,selection,active_ret ] ,
                        index=['Allocation','Selection','Active Return'],
                        columns=[date_unique[Length-1]]
                     )
                    #Final
                    return( {
                        'Raw':ari_raw ,
                        'Aggregate':ret_mat
                    } )

                else:
                    print('parms: type= should be checked twice')
                    raise ValueError


            #Sigle-period case
            else:
                #Data extract (all pd.Series)
                portret = self.Value_Brinson[ 'ret_port' ]
                benchret = self.Value_Brinson[ 'ret_bench' ]
                portwt = self.Value_Brinson[ 'weight_port' ]
                benchwt = self.Value_Brinson[ 'weight_bench' ]

                #Returns computation (Adjusted)
                cat_allocation = (portwt - benchwt) * benchret
                cat_selection = (portret - benchret) * portwt
                    

                #Result binding (the result is categorized by variable, .Info['cat_var'])
                cat_ret = pd.DataFrame( [ cat_allocation,cat_selection ] ).T
                cat_ret.columns = [ 'Allocation', 'Selection' ]
                #Add a total summary to the end row
                cat_ret = cat_ret.append( pd.DataFrame(cat_ret.sum(axis=0),columns=['Total']).T )

                #Overall Brinson Attribution
                q1 = self.Value_Brinson[ 'midret_bench' ]
                q2 = self.Value_Brinson[ 'midret_allocation' ]
                q3 = self.Value_Brinson[ 'midret_selection' ]
                q4 = self.Value_Brinson[ 'midret_port' ]
                ret_mat = pd.DataFrame( [q2-q1,q4-q2,q4-q1],columns=['Returns'],index=[ "Allocation Effect","Selection Effect","Active Return" ] )  
                
                #Return a dict
                return( {   
                    'Attribution by category in bps': cat_ret * 10000 ,
                    'Aggregate': ret_mat
                } )
            
            
            return( None )
        #========= Raise Exception ========
        else:
            print( 'parm: please check the method=' )
            raise ValueError
    

    
    def Brinson( self ):